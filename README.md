A project to collect, curate and add in all the necessary details regarding laptops supporting the Thunderbolt 3 protocol.

These details would include specs, pricing, and price comparisons.

A list of manually curated and reviewed Thunderbolt 3 laptops can be found here https://thunderboltlaptop.com/cheapest-laptops-with-thunderbolt-3/